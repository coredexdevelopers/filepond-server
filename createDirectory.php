<?php
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers: Authorization');
header('Access-Control-Allow-Methods: OPTIONS, POST');

if(isset($_POST['directoryPath'])){
	echo json_encode(createDirectory());
}

function createDirectory(){
	$directoryPath = $_POST['directoryPath'];
	$directoryPath = str_replace(" ","_",$directoryPath);
	if(!startsWith($directoryPath, '/')){
		$directoryPath = '/'.$directoryPath;
	}
	$dir = 'uploads'.$directoryPath;
	$result = mkdir($dir, 0777, true);
	return ($result);
}


function startsWith ($string, $startString) {
    $len = strlen($startString);
    return (substr($string, 0, $len) === $startString);
}
