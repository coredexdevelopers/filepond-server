# FilePond PHP Server API

A PHP server to handle [FilePond](https:pqina.nl/filepond) file uploads and [Doka](https://pqina.nl/doka) image transforms.
Customized by Adnan Hamzeh for simpler use. The documentation is kept as is, with comments on where I've made changes.


## Instructions

Comment this line in both the `index.php` and `submit.php` files to prevent posting to the server from other domains.

```php
header('Access-Control-Allow-Origin: *');
```


## Targets

The `tmp` and `upload` file paths can be configured in the `config.php` file.
Adnan's comment: in the regular filepond, the `tmp` folder is supposed to be where files are stored temporarily.
I removed all this `tmp` and `upload` separation, there's no temporary storage in my version.
What you need to do is set `TRANSFER_DIR` to the destination you want. Other values don't do anything.

```php

// where to get files from, can also be an array of fields
const ENTRY_FIELD = 'filepond';

// where to write files to
const TRANSFER_DIR = 'tmp'; // in Adnan's version, only this value matters between these 3.
const UPLOAD_DIR = 'uploads';
const VARIANTS_DIR = 'variants';

// name to use for the file metadata object
const METADATA_FILENAME = '.metadata';

```


## Image Transforms
NOT SUPPORTED IN ADNAN'S VERSION

To do image transforms on the server instead of the client we can uncomment the `require_once('config_doka.php')` line.

Transform instructions found in the `.metadata` file are now automatically applied to the first file in the upload list (when it's transfered from the transfer dir to the upload dir).


## Example

See [FilePond PHP Boiler Plate](https://github.com/pqina/filepond-boilerplate-php) for an example implementation.

## Create Directory
This feature has been added by Adnan. It does not exist in the original Filepond.
It's all in the `createDirectory.php` file, very easy. It creates a directory on the server.

## List Files
This feature has been added by Adnan. It does not exist in the original Filepond.
It's all in the `getFiles.php` file, very easy. It lists the files on the server with some metadata.

## Additional modifications
This section documents additional modifications of Filepond by Adnan.

### Sanitization
Filepond originally sanitizes strings. This has caused me issues with Arabic filenames becoming blank.
Hence, I removed the sanitization altogether. I know this is very bad, and I should fix it in the future.

### Directory Structure in TMP_DIR
Filepond originally separates newly uploaded files in a temporary directory. I don't care about this.
I want the files to be directly uploaded.
- Default behavior:
1. User uploads a file.
2. It gets uploaded to the temporary directory: `/tmp_dir/<random_folder_id_by_filepond>/users_file`

- My behavior:
1. User uploads a file.
2. It gets uploaded to the temporary directory (Which is also the permanent directory anyway): `/tmp_dir/<directory_path_supplied_as_file_metadata>/user_file`
