<?php
header('Access-Control-Allow-Origin: *');
$directoryPath = $_GET['directoryPath'];
$dir    = 'uploads'.$directoryPath;
$files = preg_grep('/^([^.])/', scandir($dir));
$files_with_metadata = [];
foreach($files as $file){
	$files_with_metadata[] = get_file_with_metadata($dir.'/'.$file);
}

echo json_encode($files_with_metadata);

function get_file_with_metadata($file){
	$server = "http://$_SERVER[HTTP_HOST]/filepond-server/";
	$fileUrl = $server.'/'.$file;
	$fileName = str_replace("uploads/", "", $file);
	return ['name' => $fileName, 'date' => filemtime($file), 'url' => $fileUrl, 'isDirectory' => is_dir($file)];
}
